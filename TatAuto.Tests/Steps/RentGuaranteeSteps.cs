﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using TatAuto.Pages;
using TechTalk.SpecFlow;


namespace TatAuto.Tests.Steps
{
    [Binding]
    class RentGuaranteeSteps
    {

        private readonly IWebDriver _driver;
        private RentGuaranteePropertyPage _rentGuaranteePage;
        private RentGuaranteeQuotePage _rentGuaranteeQuotePage;

        public RentGuaranteeSteps()
        {
            _driver = ScenarioContext.Current.Get<IWebDriver>("currentDriver");
            _rentGuaranteePage = new RentGuaranteePropertyPage(_driver);
            _rentGuaranteeQuotePage = new RentGuaranteeQuotePage(_driver);
        }

        [Given(@"I am on the Rental Guarantee Page")]
        public void GivenIAmOnTheRentalGuaranteePage()
        {

            _rentGuaranteePage.NavigateTo();
        }

        [When(@"I fill in the Rental Guarantee Property form with default data")]
        public void WhenIFillInTheRentalGuaranteePropertyFormWithDefaultPage()
        {

            _rentGuaranteePage.FillForm();
        }

        [When(@"I fill in the Zip Code with (.*) and the Rent Value with (.*)")]
        public void WhenIFillInTheZipCodeWithAndTheRentValueWith(string zipCode, string rentValue)
        {
            _rentGuaranteePage.FillForm(zipCode: zipCode, monthlyRent: rentValue);
        }


        [When(@"I click on the Simulation button")]
        public void WhenIClickOnTheSimulationButton()
        {
            _rentGuaranteePage.StartSimulation();
        }

        [Then(@"I am on the Quote Page")]
        public void ThenIAmOnTheQuotePage()
        {
            Assert.IsTrue(_rentGuaranteeQuotePage.IsOnPage(), "We are not on the Quote page after launching the simulation.");
        }

    }
}
