﻿
Feature: Create a Draft Contract on Qover.Me
	I want to create a new Draft Rental guarantee Contract

Scenario: Creating a new rental guarantee contract
	Given I am on the Rental Guarantee Page
	When I fill in the Rental Guarantee Property form with default data
	And I click on the Simulation button
	Then I am on the Quote Page


Scenario Outline: Creating new rental guarantee contract with different post codes and rents
	Given I am on the Rental Guarantee Page	
	When I fill in the Zip Code with <zipCode> and the Rent Value with <rentValue>
	And I click on the Simulation button
	Then I am on the Quote Page

	Examples:
	| zipCode | rentValue |
	| 1000    | 500       |
	| 2000    | 600       |
	| 3000    | 200       |

	