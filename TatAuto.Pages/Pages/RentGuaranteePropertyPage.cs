﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace TatAuto.Pages
{

 
    public class RentGuaranteePropertyPage
    {
        private static readonly string _homePageUrl = "https://app.qover.me/products/home/landlord?key=qrJqUGZLTVtUi8TbAGunpnzrR20OJyZS";

        [FindsBy(How = How.Id, Using = "risk.insuredProperty.address.zip")]
        protected IWebElement ZipCodeTxtBox { get; set; }

        [FindsBy(How = How.Id, Using = "risk.insuredProperty.monthlyRent.value")]
        protected IWebElement MonthlyRentTxtBox { get; set; }

        [FindsBy(How = How.Id, Using = "risk.insuredProperty.monthlyCharges.value")]
        protected IWebElement MonthlyChargesTxtBox { get; set; }

        [FindsBy(How = How.Id, Using = "risk.insuredProperty.type.residential")]
        protected IWebElement PropertyTypeResidentialOption { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id=\"content\"]/div/div[4]/div[1]/div[1]/button/span")]
        protected IWebElement SimulateBtn { get; set; }

        private readonly IWebDriver _driver;

        public enum RentalPropertyType
        {
            Residential, Commercial
        }

    

        public RentGuaranteePropertyPage(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(_driver, this);
        }

        public void NavigateTo()
        {
            _driver.Navigate().GoToUrl(_homePageUrl);

        }

        public void FillForm(string zipCode = "1000", string monthlyRent = "500", string monthlyCharges = "100", 
                             RentalPropertyType rentalPropertyType = RentalPropertyType.Residential)
        {
            ZipCodeTxtBox.SendKeys(zipCode);
            MonthlyRentTxtBox.SendKeys(monthlyRent);
            MonthlyChargesTxtBox.SendKeys(monthlyCharges);
            if (rentalPropertyType == RentalPropertyType.Residential)
            {
                PropertyTypeResidentialOption.Click();
            }

        }

        public void StartSimulation()
        {
            SimulateBtn.Click();
        }
    }
}




