﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace TatAuto.Pages
{
    public class RentGuaranteeQuotePage
    {
        private readonly IWebDriver _driver;

        // This will break pretty easily :-)
        [FindsBy(How = How.XPath, Using = "//*[@id=\"content\"]/div/div[4]/div[1]/div/div/div[1]/div/div[7]/label/span")]
        protected IWebElement BasicPlanChooseBtn { get; set; }

        public RentGuaranteeQuotePage(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(_driver, this);
        }

        public bool IsOnPage()
        {

            return BasicPlanChooseBtn.Text.ToLower().Contains("plan");

        }

    }
}
